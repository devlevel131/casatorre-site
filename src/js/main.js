var preloader = document.getElementsByClassName('preload');
$(window).ready(function () {
    $(preloader).fadeOut("slow");
});

$(document).ready(function(){
    var menu = document.getElementById("nav-icon3");
    var nav = document.getElementById("nav");
    $(menu).click(function(e){ 
       $(this).toggleClass("open");   
       $(nav).toggle("show");
       e.preventDefault();
    });
});